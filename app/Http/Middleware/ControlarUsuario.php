<?php

namespace App\Http\Middleware;

use Closure;

class ControlarUsuario
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->get('auth') || session()->get('auth')!=="autenticado") {
            return redirect()->route('login.index');
        }
        return $next($request);
    }
}
