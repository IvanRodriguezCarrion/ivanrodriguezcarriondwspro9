<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use App\Perro;
use App\Persona;

class ControladorPerro extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perros = Perro::paginate(3);
        return view('leerperro')->with('perros',$perros);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = $this->lastId();
        $personas = Persona::all();

        return view('crearperro')->with("id",$id)->with('personas', $personas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $perros  = new Perro($request->all());      
        $perros->save();
        
        return redirect()->route('perros');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->buscarid;
        
        if(Perro::find($id)){
            $perro  = Perro::find($id);
            $perro->persona;
            $perros = array();
            array_push($perros, $perro);
            return view('leerperro')->with('perros',$perros);
            
        } else {
            flash('El perro con id '.$id.' no se ha encontrado',"danger");
            return redirect()->route('perros');
        }
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $perro  = Perro::find($id);
        $personas = Persona::all(); 
        return view('editarperro')->with("perro",$perro)->with('personas', $personas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $perro  = Perro::find($id);
        $perro->nombre = $request->nombre;
        $perro->raza = $request->raza;
        $perro->nChip = $request->nombre;
        $perro->persona_id = $request->persona_id;
        
        $perro->update();
        flash('El perro '.$perro->nombre.' ha sido editado con éxito', 'warning');
        
        return redirect()->route('perros');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $perro  = Perro::find($id);
        $perro->delete();
        flash('El perro '.$perro->nombre.' ha sido borrado con éxito', 'danger');
        return redirect()->route('perros');
    }
    
    public function lastId()
    {
        $id = DB::select("SELECT MAX(id) AS id FROM perros");
        $id = $id[0]->id;
        $id++;
        return $id;
    }
}
