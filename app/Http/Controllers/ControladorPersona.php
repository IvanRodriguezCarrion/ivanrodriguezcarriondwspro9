<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Persona;

class ControladorPersona extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personas = DB::table("personas")->paginate(5);
        
        return view('leerpersona')->with('personas',$personas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = $this->lastId();
        
        return view('crearpersona')->with("id",$id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $persona  = new Persona($request->all());      
        $persona->save();
        
        return redirect()->route('personas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $id = $request->buscarid;
        if(Persona::find($id)){
            $persona  = Persona::find($id);
            $personas = array();
            array_push($personas, $persona);
            return view('leerpersona')->with('personas',$personas);
        } else {
            flash('La persona con id '.$id.' no se ha encontrado',"danger");
             return redirect()->route('personas');
        }
       
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $persona  = Persona::find($id);
        return view('editarpersona')->with("persona",$persona);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $persona  = Persona::find($id);
        $persona->nombre = $request->nombre;
        $persona->apellidos = $request->apellidos;
        $persona->save();
        flash('La persona '.$persona->nombre.' ha sido editada con éxito', 'warning');
        $personas = DB::table("personas")->paginate(10);
        
        return view('leerpersona')->with('personas',$personas);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $persona = Persona::find($id);
        $persona->delete();
        flash('La persona '.$persona->nombre.' ha sido borrado con éxito', 'danger');
        return redirect()->route('personas');
    }
    
    public function lastId()
    {
        $id = DB::select("SELECT MAX(id) AS id FROM personas");
        $id = $id[0]->id;
        $id++;
        return $id;
    }
    
    
}
