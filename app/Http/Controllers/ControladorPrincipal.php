<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ControladorPrincipal extends Controller
{
    protected $usuario = "admin";
    protected $pass = "admin";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('indice');
    }
    
    public function login()
    {
        return view('login');
        
    }
    
    
    public function check(Request $request)
    {

        if ($request->nombre == $this->usuario && $request->pass == $this->pass) {
            $request->session()->put('auth', 'autenticado');
            return redirect()->route('indice');
        } else {
            flash('El usuario introducido no coincide con uno autorizado',"danger");
            return redirect()->route('login.index');
        }
        
    }
    
    public function salir()
    {
        session()->forget('auth');
        return redirect()->route('login.index');
        
    }

}
