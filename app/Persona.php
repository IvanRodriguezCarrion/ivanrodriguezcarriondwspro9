<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    public $timestamps = false;
    protected $tabla = "personas";
    protected $fillable = ["id","nombre","apellidos"];
    
    public function perros() {
        return $this->hasMany('App\Perro');
    }
}
