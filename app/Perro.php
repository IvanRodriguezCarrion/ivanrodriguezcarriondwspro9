<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perro extends Model
{
    public $timestamps = false;
    protected $tabla = "perros";
    protected $fillable = ["id", "nombre","raza","nChip","persona_id"];
    
    public function persona() {
        return $this->belongsTo('App\Persona','persona_id');
    }
}
