<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',[
    'as' => 'indice',
    'uses' => 'ControladorPrincipal@index'
])->middleware("log");

Route::get("personas", [
    'as' => 'personas',
    'uses' => 'ControladorPersona@index'
])->middleware("log");

Route::get("personas/borrar/{id}", [
    'as' => 'personas.destroy',
    'uses' => 'ControladorPersona@destroy'
])->middleware("log");

Route::get("personas/editar/{id}", [
    'as' => 'personas.edit',
    'uses' => 'ControladorPersona@edit'
])->middleware("log");

Route::get("personas/crear", [
    'as' => 'personas.create',
    'uses' => 'ControladorPersona@create'
])->middleware("log");

Route::post("personas/guardar", [
    'as' => 'personas.store',
    'uses' => 'ControladorPersona@store'
])->middleware("log");

Route::get("personas/actualizar/{id}", [
    'as' => 'personas.update',
    'uses' => 'ControladorPersona@update'
])->middleware("log");

Route::get("personas/mostrar", [
    'as' => 'personas.show',
    'uses' => 'ControladorPersona@show'
])->middleware("log");

Route::get("perros", [
    'as' => 'perros',
    'uses' => 'ControladorPerro@index'
])->middleware("log");

Route::get("perros/borrar/{id}", [
    'as' => 'perros.destroy',
    'uses' => 'ControladorPerro@destroy'
])->middleware("log");

Route::get("perros/editar/{id}", [
    'as' => 'perros.edit',
    'uses' => 'ControladorPerro@edit'
])->middleware("log");

Route::get("perros/crear", [
    'as' => 'perros.create',
    'uses' => 'ControladorPerro@create'
])->middleware("log");

Route::post("perros/guardar", [
    'as' => 'perros.store',
    'uses' => 'ControladorPerro@store'
])->middleware("log");

Route::get("perros/actualizar/{id}", [
    'as' => 'perros.update',
    'uses' => 'ControladorPerro@update'
])->middleware("log");

Route::get("perros/mostrar", [
    'as' => 'perros.show',
    'uses' => 'ControladorPerro@show'
])->middleware("log");

Route::get("/login", [
    'as' => 'login.index',
    'uses' => 'ControladorPrincipal@login'
]);
Route::POST("/login/comprobar", [
    'as' => 'login.check',
    'uses' => 'ControladorPrincipal@check'
]);

Route::get("/login/salir", [
    'as' => 'login.salir',
    'uses' => 'ControladorPrincipal@salir'
])->middleware("log");