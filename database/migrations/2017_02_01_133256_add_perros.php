<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPerros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('perros')) {
            Schema::create('perros', function(Blueprint $tabla) {
                $tabla->increments('id');
                $tabla->string('nombre');
                $tabla->string('raza');
                $tabla->string('nChip');
                $tabla->integer('persona_id')->unsigned();
                $tabla->foreign('persona_id')->references('id')->on('personas')->onDelete('cascade');
            });
        }
         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
