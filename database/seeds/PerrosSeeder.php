<?php

use Illuminate\Database\Seeder;

class PerrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       for ($i = 1; $i < 4; $i++) {
            DB::table('perros')->insert(array(
            'nombre' => 'Perro' . $i,
            'raza' => 'Raza' . $i,
            'nChip' => rand(10000,99999),
            'persona_id' => $i
            ));
    }
}
}

