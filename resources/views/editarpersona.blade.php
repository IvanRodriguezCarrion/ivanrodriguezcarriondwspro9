@extends('layout')

@section('titulo', "Editar persona ".$persona->nombre)

@section('contenido')

<div class="col-md-offset-2 col-md-8 ">
<form action="{{route('personas.update', $persona->id)}}" method="get" >
    
    <div class="form-group">
        <label>Identificador</label>
        <input class="form-control" type="text" name="id" value="{{$persona->id}}" readonly="readonly"/>
    </div>
	<div class="form-group">
        <label>Nombre</label>
        <input class="form-control" type="text" name="nombre" value="{{$persona->nombre}}"/>
    </div>    
    <div class="form-group">
        <label>Apellidos</label>
        <input class="form-control" type="text" name="apellidos" value="{{$persona->apellidos}}"/>
    </div>
	<div class="form-group pull-right">
        <button class="btn btn-default" type="submit">Actualizar</button> 
    </div>
    <input type="hidden" name="_method" value="delete" /> <!--IMPORTANTE para hacer funcionar el método store tal y como está puesto aquí -->
</form>
</div>

@endsection