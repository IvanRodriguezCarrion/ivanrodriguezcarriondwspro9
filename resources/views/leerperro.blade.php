@extends('layout')

@section('titulo', "Lista de perros")

@section('contenido')

<div class="pull-right col-md-12">
    <form action="{{route('perros.show')}}" method="get">
        <div class="input-group ">
          <input type="text" class="form-control" placeholder="Introduzca ID" name="buscarid">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-default">Buscar</a>
          </span>
        </div>
    </form>
</div>

<table class="table table-striped">
    <thead>
    <th>ID</th>
    <th>Nombre</th>
    <th>Raza</th>
    <th>Número Chip</th>
    <th>Propietario</th>
    <th>Acción</th>
</thead>
<tbody>
    @foreach ($perros as $perro)
        <tr>
            <td>{{ $perro->id }}</td>
            <td>{{ $perro->nombre }}</td>
            <td>{{ $perro->raza }}</td>
            <td>{{ $perro->nChip }}</td>
            <td>
                {{ $perro->persona->nombre }}
            </td>
            <td>
            <div class="btn btn-group"><a class="btn btn-default" href="{{route('perros.edit', $perro->id)}}">Editar</a>
            <a class="btn btn-default" href="{{route('perros.destroy', $perro->id)}}">Borrar</a></div>
            </td>
        </tr>
    @endforeach
</tbody>
</table>
@if (count($perros)>1)
<div class="pull-right">{{ $perros->links() }}</div>
@endif

@endsection