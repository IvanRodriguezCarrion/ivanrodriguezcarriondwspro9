@extends('layout')

@section('titulo', "Nuevo perro")

@section('contenido')
<div class="col-md-offset-2 col-md-8 col-md-offset-2">
<form action={{route('perros.store')}} method="post" >
    <div class="form-group">
        <label>Identificador</label>
        <input class="form-control" type="text" name="id" value="{{$id}}" readonly="readonly"/>
    </div>
    <div class="form-group">
        <label>Nombre</label>
        <input class="form-control" type="text" name="nombre" value="" required/>
    </div>
    <div class="form-group">
        <label>Raza</label>
        <input class="form-control" type="text" name="raza" value="" required/>
    </div>
    <div class="form-group">
        <label>Número de chip</label>
        <input class="form-control" type="text" name="nChip" value="" required/>
    </div>
    <div class="form-group">
        <label>Propietario</label>
        <select class="form-control" name="persona_id">
		    <option value ="0"> Escoja un propietario </option>
		    @foreach ($personas as $persona)
    		    <option value="{{$persona->id}}">
			    {{$persona->nombre}}
    		    </option>
		    @endforeach
		</select>
    </div>
	    
	<div class="form-group pull-right">
        <button class="btn btn-default" type="submit">Guardar</button> 
    </div>
    
    <input type="hidden" name="_token" value="{{ csrf_token() }}"> <!--IMPORTANTE para hacer funcionar el método store tal y como está puesto aquí -->

</form>
    </div>

@endsection