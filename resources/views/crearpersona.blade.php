@extends('layout')

@section('titulo', "Nueva persona")

@section('contenido')

<div class="col-md-offset-2 col-md-8 col-md-offset-2">
<form action="{{route('personas.store')}}" method="post" >
    <div class="form-group">
        <label>Identificador</label>
        <input class="form-control" type="text" name="id" value="{{$id}}" readonly="readonly"/>
    </div>
    <div class="form-group">
        <label>Nombre</label>
        <input class="form-control" type="text" name="nombre" value=""/>
    </div>
    <div class="form-group">
        <label>Apellidos</label>
        <input class="form-control" type="text" name="apellidos" value=""/>
    </div>
    <div class="form-group pull-right">
        <button class="btn btn-default" type="submit">Guardar</button> 
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}"> <!--IMPORTANTE para hacer funcionar el método store tal y como está puesto aquí -->
</form>
</div>


@endsection