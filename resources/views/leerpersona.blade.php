@extends('layout')

@section('titulo', "Lista de personas")

@section('contenido')

<div class="pull-right col-md-12">
    <form action="{{route('personas.show')}}" method="get">
        <div class="input-group ">
          <input type="text" class="form-control" placeholder="Introduzca ID" name="buscarid">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-default">Buscar</a>
          </span>
        </div>
    </form>
</div>


<table class="table table-striped">
    <thead>
    <th>ID</th>
    <th>Nombre</th>
    <th>Apellido</th>
    <th>Acción</th>
</thead>
<tbody>
    @foreach ($personas as $persona)
        <tr>
            <td>{{ $persona->id }}</td>
            <td>{{ $persona->nombre }}</td>
            <td>{{ $persona->apellidos }}</td>
            <td>
            <div class="btn btn-group"><a class="btn btn-default" href="{{route('personas.edit', $persona->id)}}">Editar</a>
            <a class="btn btn-default" href="{{route('personas.destroy', $persona->id)}}">Borrar</a></div>
            </td>
        </tr>
    @endforeach
</tbody>
</table>
@if (count($personas)>1)
<div class="pull-right">{{ $personas->links() }}</div>
@endif
@endsection