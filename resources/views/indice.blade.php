@extends('layout')

@section('titulo', "Página Principal")

@section('contenido')
<div class="col-md-offset-1 col-md-10 col-md-offset-1"><h2>¡BIENVENIDO A LA APP DE APADRINAMIENTO DE RINGO! ^^</h2></div>

<div class="col-md-offset-4 col-md-4 col-md-offset-4"> 
<img class="img-responsive" style = "height:70vh" src="{{ asset('images/Ringo.png') }}"/>
</div>
@endsection