@extends('layout')

@section('titulo', "Editar perro ".$perro->nombre)

@section('contenido')
<div class="col-md-offset-2 col-md-8 col-md-offset-2">
<form action="{{route('perros.update', $perro->id)}}" method="get" >
        <div class="form-group">
        <label>Identificador</label>
        <input class="form-control" type="text" name="id" value="{{$perro->id}}" readonly="readonly"/>
    </div>
    <div class="form-group">
        <label>Nombre</label>
        <input class="form-control" type="text" name="nombre" value="{{$perro->nombre}}" required/>
    </div>
    <div class="form-group">
        <label>Raza</label>
        <input class="form-control" type="text" name="raza" value="{{$perro->raza}}" required/>
    </div>
    <div class="form-group">
        <label>Número de chip</label>
        <input class="form-control" type="text" name="nChip" value="{{$perro->nChip}}" required/>
    </div>
	<div class="form-group">
        <label>Propietario</label>
        <select class="form-control" name="persona_id">
		    <option value ="0"> Escoja un propietario </option>
		    @foreach ($personas as $persona)
            @if ($perro->persona_id == $persona->id)
    		    <option value="{{$persona->id}}" selected>
            @else
                <option value="{{$persona->id}}">
            @endif
			    {{$persona->nombre}}
    		    </option>
		    @endforeach
		</select>
    </div>
	    
	<div class="form-group pull-right">
        <button class="btn btn-default" type="submit">Guardar</button> 
    </div>
    <input type="hidden" name="_method" value="delete" /> <!--IMPORTANTE para hacer funcionar el método store tal y como está puesto aquí -->
</form>
</div>

@endsection