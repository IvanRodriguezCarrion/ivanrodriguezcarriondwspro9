<!DOCTYPE html>
<html lang="es">
    <head>
        <title>@yield('titulo','Apadrinamiento') | Aplicación de apadrinamiento</title>
        <meta charset="UTF-8">
        <link rel="shorcut icon" href="{{ asset('images/ringo.ico') }}">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
        <style>
            body {background: url("{{ asset('images/fondo.png') }}"),repeat;}
        </style>
    </head>
    <body >
     <div class="container">   
	<nav class="navbar navbar-default navbar-static-top">
	    <ul class="nav nav-pills nav-justified">
        <li><a href="{{route('indice')}}">Inicio</a></li>
		<li><a href="{{route('personas.create')}}">Alta de padrino</a></li>
		<li><a href="{{route('personas')}}">Nuestros padrinos</a></li>
		<li><a href="{{route('perros.create')}}">Alta de perro</a></li>
		<li><a href="{{route('perros')}}">Nuestros perros</a></li>
        <li><a href="{{route('login.salir')}}">Salir</a></li>
	    </ul>
	</nav>
        
    
        @if (session()->has('flash_notification.message'))
            <div class="alert alert-{{ session('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!! session('flash_notification.message') !!}
            </div>
        @endif
        
        @yield('contenido')
        
	

	<footer class="col-md-12">
	    <hr/><pre> Ringo S.L - Ivan Rodriguez Carrion - Desarrollo Web Entorno Servidor - 01-02-2017</pre>
	</footer>
         </div>
    </body>
</html>