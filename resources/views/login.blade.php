@extends('layout')

@section('titulo', "Login")

@section('contenido')

<div class="col-md-offset-2 col-md-8 col-md-offset-2">
    <form action={{route('login.check')}} method="post" >
        <h3>Comprobación de usuario<h3>
        <div class="form-group">
            <label>Nombre</label>
            <input class="form-control" type="text" name="nombre" value="admin" required/>
        </div>
        <div class="form-group">
            <label>Contraseña</label>
            <input class="form-control" type="password" name="pass" value="admin" required/>
        </div>
        <div class="form-group pull-right">
            <button class="btn btn-default" type="submit">Acceder</button> 
            <button class="btn btn-default" type="reset">Borrar</button> 
        </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}"> <!--IMPORTANTE para hacer funcionar el método store tal y como está puesto aquí -->
    </form>
</div>
@endsection